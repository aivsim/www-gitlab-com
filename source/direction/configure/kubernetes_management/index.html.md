---
layout: markdown_page
title: "Category Direction - Kubernetes Management"
description: "GitLab aim to provide a simple way for users to configure their clusters and manage Kubernetes. Learn more here!"
canonical_path: "/direction/configure/kubernetes_management/"
---

- TOC
{:toc}

## Overview

The main motivating factor behind [using a container orchestration platform is cost reduction](https://medium.com/kubernetes-tutorials/how-can-containers-and-kubernetes-save-you-money-fc66b0c94022). Kubernetes has become the most widely adopted container orchestration platform to run, deploy, and manage applications. Yet, to seamlessly connect application code with the clusters in various environments, has remained a complex task that needs various approaches of CI/CD and Infrastructure as Code. 

Our mission is to support the Platform Engineers in enabling developer workflows, to make deployments to every environment frictionless for the developer and reliable for the operator, no matter the experience level. We do this by supporting an integrated experience within GitLab and leverage the existing tools and approaches in the Kubernetes community.

### Market overvoew

Kubernetes is emerging as the _de facto_ standard for container orchestration. Today, Kubernetes runs in half of container environments, and around 90% of containers are orchestrated. ([Source](https://www.datadoghq.com/container-report/)).

### How you can contribute

Interested in joining the conversation for this category? Please have a look at our
[global epic](https://gitlab.com/groups/gitlab-org/-/epics/115) where
we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

## Vision

In two years, GitLab is an indispensable addition in the workflow of Kubernetes users by making deployments reliable, frictionless and conventional. As we are primarily focusing on enterprise customers, we want to support highly regulated, air-gapped use cases along with generic, public-cloud based environments.

## Target User

We are building GitLab's Kubernetes Management category for the enterprise operators, who are already operating production grade clusters and work hard to remove all the roadblocks that could slow down product development without putting operations at risk.

- [Priyanka](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)

## What's next & why

### GitLab agent

Using the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/), we want to offer a security oriented, Kubernetes-friendly integration option to Platform Engineers. Beside providing more control for the cluster operator, the agent opens up many new possibilities to make GitLab - cluster integrations easier and deeper. We expect the agent to support various GitLab features with automatic setups and integrations.

We are currently working on:

- [enabling GitLab Kubernetes Agent support on gitlab.com](https://gitlab.com/groups/gitlab-org/-/epics/3834)
- [adding support for private projects](https://gitlab.com/gitlab-org/gitlab/-/issues/220912)
- [adding support for repository based deployments](https://gitlab.com/gitlab-org/gitlab/-/issues/259669)

We are preparing

- support for [review applications with the Agent](https://gitlab.com/gitlab-org/gitlab/-/issues/273656)
- show [synced status of manifest projects](https://gitlab.com/gitlab-org/gitlab/-/issues/258603)
- show [administrative information around the agents](https://gitlab.com/groups/gitlab-org/-/epics/4739)

Please, [join the discussion](https://gitlab.com/gitlab-org/gitlab/-/issues/216569).

### GitLab Managed Applications

[GitLab Managed Applications](https://docs.gitlab.com/ee/user/clusters/applications.html) are currently undergoing a large development by moving from the UI-only setup, to a more customizable, CI/CD-based, Infrastructure as Code setup. We want to allow our users to install off the shelf applications with necessary customizations. Moreover, we will continue to offer easy installations of the applications needed to benefit from GitLab's features like the Runner, Auto DevOps, Metrics and log collection, etc.

While this approach would allow it, we do not intend to create a marketplace of apps inside of GitLab, instead we would like to integrate with solutions like [OperatorHub](https://operatorhub.io/). As a result, we are looking at providing apps as GitLab Managed Apps if they satisfy the following criteria:

- Each GitLab Managed App should provide sane, production-ready default configurations.
- Each GitLab Managed App should be a requirement for current or planned GitLab functionality or it should integrate deeply with GitLab.
- Each GitLab Managed App should have a clear support path, possibly outside of GitLab.

While we build out an IaC driven approach to GitLab Managed Apps, we are [deprecating the existing approach to GitLab Managed Applications](https://gitlab.com/groups/gitlab-org/-/epics/4280) in order to provide an experience that fits the above requirements and is more customizable.

Contribute to these directions in the Epic ["Create CI templates for GitLab-managed-apps"](https://gitlab.com/groups/gitlab-org/-/epics/2103)

## Challenges

We see Kubernetes as a platform for delivering containerised software to users. Currently, every company builds its own workflows on top of it. The biggest challenges as we see them are the following:

- Making Kubernetes friendly to developers is hard. We want to make deployments to every Kubernetes environment frictionless for the developer and reliable for the operator.
- As clusters are complex having an overview of the state and contents of a cluster is hard. We want to help our [monitoring efforts](/direction/monitoring) to provide clean visualisation and a rich understanding of one's cluster.

## Approach

- Kubernetes has a large and engaged community, and we want to build on the knowledge and wisdom of the community, instead of re-inventing existing solutions.
- In Kubernetes there are a plethora of ways for achieving a given goal. We want to provide a default setup and configuration options to integrate with all popular approaches and tools.

## Maturity

Kubernetes Management is currently `Viable`. We want to make it `Complete` by February 2021.

## Competitive landscape

### IBM Cloud Native Toolkit

It provides an Open Source Software Development Life Cycle (SDLC) and complements IBM Cloud Pak solutions. The Cloud Native Toolkit enables application development teams to deliver business value quickly using Red Hat OpenShift and Kubernetes on IBM Cloud.

### Spinnaker

Spinnaker is an open-source, multi-cloud continuous delivery platform that helps you release software changes with high velocity and confidence. It provides two core sets of features 1) Application management, and 2) Application deployment.

## Analyst landscape

This category doesn't quite fit the "configuration management" area as it relates only to Kubernetes. No analyst area currently defined.

## Top Customer Success/Sales issue(s)

[Customize Kubernetes namespace per environment for managed clusters](https://gitlab.com/gitlab-org/gitlab/-/issues/38054)

## Top user issue(s)

[Customize Kubernetes namespace per environment for managed clusters](https://gitlab.com/gitlab-org/gitlab/-/issues/38054)

## Top internal customer issue(s)

[Add more configuration options for launching a new cluster directly from the GitLab application](https://gitlab.com/gitlab-org/gitlab/-/issues/30876)

## Top Vision Item(s)

- [Kubernetes Management - enterprise level K8s integration](https://gitlab.com/gitlab-org/gitlab/-/issues/216569)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)
