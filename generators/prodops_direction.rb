require_relative '../lib/gitlab/file_cache'
require 'active_support/core_ext/date/calculations'
require 'active_support/core_ext/numeric/time'
require 'active_support/notifications'

require 'colorize'
require 'generators/direction'

module Generators
  class ProdOpsDirection < Direction
    class MilestoneFilter < Direction::MilestoneFilter
      attr_accessor :filtering_date, :milestones_by_expired_state

      def initialize(milestones)
        self.filtering_date = Date.today.months_ago(2)
        self.milestones_by_expired_state =
          milestones
            .map(&method(:parse_due_date))
            .group_by(&method(:expired_state))
            .transform_values(&method(:sort_by_due_date))
      end
    end

    class GitLabProject < Direction::GitLabProject
      def milestone_direction_issues(milestone_id, prod_label)
        result = []
        result += @instance.get("projects/#{@id}/issues", true, milestone: milestone_id, labels: 'prodops:direction,Product Operations,' + "prodops:#{prod_label}", confidential: 'false', per_page: 100)
        result
      end
    end

    class GitLabGroup < Direction::GitLabGroup
      def milestones(state = 'active')
        result = @instance.get("groups/#{@id}/milestones", true, state: state, per_page: 100)

        MilestoneFilter.new(result)[state]
      end

      # finds all Direction epics that have a fixed or inherited due date that falls
      # between the given milestone start and end dates
      def milestone_direction_epics(milestone, prod_label)
        epics = []
        results = []
        epics += @instance.get("groups/#{@id}/epics", true, labels: 'prodops:direction,Product Operations,' + "prodops:#{prod_label}", confidential: 'false', per_page: 100)
        epics.each do |epic|
          next if epic['due_date_from_inherited_source'].nil? && epic['due_date'].nil?

          if !epic['due_date_from_inherited_source'].nil? && (epic['due_date_from_inherited_source'] > milestone['start_date']) && (epic['due_date_from_inherited_source'] <= milestone['due_date'])
            results.push(epic)
            next
          end
          next if epic['due_date'].nil?

          if (epic['due_date'] > milestone['start_date']) && (epic['due_date'] <= milestone['due_date'])
            results.push(epic)
          end
        end
        results
      end
    end

    PROD_OPS = ['Product Operations'].freeze # this is not a real stage but used to generate Product Operations pages

    # set your stage to 'true' to include Epics with Direction label
    INCLUDE_EPICS = Hash.new("false")
    INCLUDE_EPICS['Product Operations'] = true
    INCLUDE_EPICS.freeze

    # special labels for Product Operations areas of strategic focus
    LABELS = %w[knowledge feedbackloops outcomes].freeze

    def format_direction_header(title, due_date)
      # check if the release number can cast to float (i.e., 11.1, 11.12, etc.)
      # this will be used to not display a date for non-matching, but included
      # milestones such as "Next 3-4 releases"
      begin
        if Float(title)
          # was able to cast to float, looks like a normal release number
          # milestone due date is actually code freeze, so we need to add 5 days so we display the correct date
          display_date = Date.parse(due_date) + 5.days
          output = "##### #{title} (" + display_date.strftime('%Y-%m-%d') + ")\n\n"
        end
      rescue ArgumentError
        # milestone title is not a normal release number, do not include extra date info
        output = "##### #{title}\n\n"
      end
      output
    end

    def get_direction_issues(project, milestone, prod_label)
      issues = []
      issues += project.milestone_direction_issues(milestone['title'], prod_label)
      issues
    end

    def get_issues(edition, milestone, prod_label)
      issues = []
      edition.each do |project|
        issues += get_direction_issues(project, milestone, prod_label)
      end
      issues
    end

    def get_direction_buckets(issues, epics)
      buckets = {}
      issues.each do |issue|
        buckets["Product Operations"] = {} if buckets["Product Operations"].nil?
        buckets["Product Operations"]['issues'] = [] if buckets["Product Operations"]['issues'].nil?
        buckets["Product Operations"]['issues'] << issue if issue['labels'].include?("Product Operations")
      end
      epics.each do |epic|
        buckets["Product Operations"] = {} if buckets["Product Operations"].nil?
        buckets["Product Operations"]['epics'] = [] if buckets["Product Operations"]['epics'].nil?
        buckets["Product Operations"]['epics'] << epic if epic['labels'].include?("Product Operations")
      end
      buckets
    end

    def fill_epics(milestone, buckets)
      direction_epics = ''
      buckets["Product Operations"]['epics'].each do |epic|
        direction_epics += tier_bullet(epic)
      end
      direction_epics
    end

    def fill_issues(milestone, buckets)
      direction_issues = ''
      buckets["Product Operations"]['issues'].each do |issue|
        direction_issues += tier_bullet(issue)
      end
      direction_issues
    end

    def fill_recent(include_epics, prod_label)
      direction_recent = ''
      # grabs at most 2 milestones that are still 'active' but have expired (past due date)
      recent_org = []
      recent_com = []
      recent_org += gitlaborg.milestones('active').select { |m| m['title'].match(/\d+\.\d+/) && m['expired'] == true }.reverse
      recent_com += gitlabcom.milestones('active').select { |m| m['title'].match(/\d+\.\d+/) && m['expired'] == true }.reverse

      recent = 0
      recent_org.each do |ms|
        next if recent >= 2

        issues = get_issues(edition_prodops, ms, prod_label)
        epics = gitlaborg.milestone_direction_epics(ms, prod_label)
        recent_com.each do |ms_com|
          if ms_com['title'] == ms['title']
            epics += gitlabcom.milestone_direction_epics(ms_com, prod_label)
          end
        end

        next if issues.empty? && epics.empty?

        direction_recent += format_direction_header(ms['title'], ms['due_date'])

        # run through all issues and epics and throw in relevant bucket (stage)
        buckets = get_direction_buckets(issues, epics)

        # Now unfold the bins in order, so all the epics and issues are under their stage
        # Epics will come before issues for stages set to true in INCLUDE_EPICS hash
        next unless buckets["Product Operations"].count.positive?

        direction_recent += fill_epics(ms, buckets) unless buckets["Product Operations"]['epics'].nil? || (include_epics["Product Operations"] == false)
        direction_recent += fill_issues(ms, buckets) unless buckets["Product Operations"]['issues'].nil?
        direction_recent += "\n"
        recent += 1
      end
      direction_recent
    end

    # this is specifically for Product Operations direction items, no other stages
    # rubocop: disable Metrics/AbcSize
    def generate_prod_ops_direction(stages, include_epics)
      direction_output = Hash.new { |hash, key| hash[key] = Hash.new(&hash.default_proc) }

      LABELS.each do |prod_label|
        direction_output[prod_label]["all"] = ''
        direction_output[prod_label]["recent"] = fill_recent(include_epics, prod_label)
      end

      gitlaborg.milestones.each do |ms|
        # subtract 5 days because the release's milestone due date is set to the freeze, not release date
        next unless ms['due_date'] && Date.parse(ms['due_date']) >= Date.today - 5.days

        LABELS.each do |prod_label|
          issues = get_issues(edition_prodops, ms, prod_label)
          epics = gitlaborg.milestone_direction_epics(ms, prod_label)
          gitlabcom.milestones.each do |ms_com|
            if ms_com['title'] == ms['title']
              epics += gitlabcom.milestone_direction_epics(ms_com, prod_label)
            end
          end

          next if issues.empty? && epics.empty?

          direction_output[prod_label]["all"] += format_direction_header(ms['title'], ms['due_date'])

          # run through all issues and epics and throw in relevant bucket (stage)
          buckets = get_direction_buckets(issues, epics)

          # Now unfold the bins in order, so all the epics and issues are under their stage
          # Epics will come before issues for stages set to true in INCLUDE_EPICS hash
          next unless buckets["Product Operations"].count.positive?

          direction_output[prod_label]["all"] += fill_epics(ms, buckets) unless buckets["Product Operations"]['epics'].nil? || (include_epics["Product Operations"] == false)
          direction_output[prod_label]["all"] += fill_issues(ms, buckets) unless buckets["Product Operations"]['issues'].nil?

          direction_output[prod_label]["all"] << "\n"
        end
      end
      direction_output
    end
    # rubocop: enable Metrics/AbcSize

    private

    def tier_bullet(obj)
      output = "- [#{obj['title']}](#{obj['web_url']})\n"
      output
    end

    def edition_prodops
      @edition_prodops ||= begin
        com = Generators::Direction::GitLabInstance.new('GitLab.com')
        gitlab_org = GitLabProject.new('gitlab-org/gitlab', com)
        product = GitLabProject.new('gitlab-com/Product', com)
        gitlab_com = GitLabProject.new('gitlab-com/www-gitlab-com', com)
        [gitlab_org, product, gitlab_com]
      end
    end

    def gitlaborg
      @gitlaborg ||= begin
        com = Generators::Direction::GitLabInstance.new('GitLab.com')
        GitLabGroup.new('gitlab-org', com)
      end
    end

    def gitlabcom
      @gitlabcom ||= begin
        com = Generators::Direction::GitLabInstance.new('GitLab.com')
        GitLabGroup.new('gitlab-com', com)
      end
    end
  end
end
