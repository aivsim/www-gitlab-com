---
layout: handbook-page-toc
title: "Digital Marketing Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Digital Marketing Managers
Digital Marketers are responsible for all inbound marketing.

## DMP labels in GitLab

*Note*: Some of the following labels only exist on the Digital Marketing Programs project level.

* **Digital Marketing Programs**: General label to track all issues related to Digital Marketing Programs
* **SEM**: Used for issues that require organic and paid search initiatives
* **Paid Ads**: Used for any paid advertising campaign such as Google Ads
* **Paid Social**: Used for paid social campaigns such as LinkedIn InMail and Facebook Ads 

## DMP Slack channels

*  `#digital-marketing`: General digital marketing conversation and questions
*  `#dmpteam`: Discussion for DMP team members

## Paid Digital Marketing 

### Goals and objectives accomplished with digital campaigns:
* Lead Generation
* Brand Awareness
* Event Registrations
* Webcast Views
* Content Downloads
* Website Traffic

### How does paid digital contribute to GitLab’s funnel?
* Top of Funnel: Introduce GitLab brand to potential customers with awareness and reach objectives.
* Middle of Funnel: Nurturing engaged prospects with educational, assets, consideration-stage content and events.
* Bottom of Funnel: Leading prospects to conversion by retargeting with relevant, personalized ad experiences.

### Working with PMG, our Digital agency
All digital campaigns are executed with the help of our digital agency PMG (learn more about PMG [here](https://www.pmg.com/)). The only exception is account-centric Paid Display which is handled directly through [Demandbase](/handbook/marketing/revenue-marketing/account-based-marketing/demandbase/), but PMG still handles regular Paid Display. For GitLab digital initiatives, PMG provides:

* Strategy & planning - Covering full-funnel paid strategy both holistically and granularly, considering the GitLab brand and audience for all channels and tactics.
* Buying & executing - Gathering all necessary campaign materials (goals & objectives, creative, copy, targeting, etc) to launch, and strategically bidding/negotiating for ad units/packages and optimizing for more efficient costs towards goals throughout campaigns.
* Insights & reporting - Aggregating all digital performance into custom report dashboards, offering actionable insights for audience, asset, regional, and  campaign performance within each issue, and reviewing overall performance over time to uncover trends & opportunities.
* Billing - Handling all paid digital spend directly with publishers & platforms, then syncing with GitLab finance to manage invoicing.

GitLab DMPs work directly with PMG within the GitLab [Digital Advertising project](https://gitlab.com/gitlab-com/marketing/digital-advertising), which is strictly for DMP & PMG communication only. To learn how PMG is involved in the paid digital request process, please refer to the [DMP Request Workflow](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#dmp-request-workflows) section below.

### Digital Campaign Types
DMPs can recommend specific types based on your campaign goals. The most common type is Paid Social based on robust targeting criteria and successful performance in reach and lead volume.

#### Channels
* Paid Social
   - LinkedIn (Sponsored Content Ads, Carousel Ads, InMail/Message Ads, and Conversation Ads)
   - Facebook (Single Image & Video Ads, Lead Generation Ads, Carousel Ads)
   - Instagram (Single Image & Video Ads, Lead Generation Ads, Carousel Ads)
   - Twitter (Promoted Tweets, Image or Video)
* Paid Search (Google and/or Bing)
* Paid Display (Google Display Network)
   - Demandbase is a digital marketing platform for Display only, mainly used to target specific accounts. Please note that Demandbase is not managed by PMG, but with Demandbase directly. Our ABM team (focuses on Tier 1,2,3 accounts) and Field Marketing team (focuses on named accounts) are the main users of DemandBase. Check out the [DemandBase page](/handbook/marketing/revenue-marketing/account-based-marketing/demandbase/) for more info. 
* Content Syndication
* Sponsorships: 
   - Virtual Event (Virtual Conference, Panel, Talking Head, All-Day Event/Summit)
   - Custom Webcasts (single & multi-vendor)
   - Microsites
   - Newsletter ads
   - Custom email blasts
   - Sponsored Custom & 3rd Party Content Creation (trend reports, ebooks, articles, etc.)


### Program definitions

#### **Paid Search**: 
Paid search are [text ads](https://support.google.com/google-ads/answer/1704389?hl=en) on Google and/or Bing search engines to drive people to specific GitLab landing pages as they are looking for information on search engines. We do this by [bidding](https://support.google.com/google-ads/answer/2459326?hl=en) on targeted keywords and phrases based on the assumed intent of the person and matching that intent with a related landing page. 
* **Best used for**: Mid and bottom funnel content where we want someone to take action (ex: filling our a form to a gated asset, signing up for a demo, etc).
     * **Best type of content to use**: Use case type gated assets that directly applies to the intent for the search query. How-to guides and ebooks do well here. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports that are not specifically tied to the intent of a search query. Caveat here is if the content is more of a how to report or guide. 
NOTE: Facebook has a one week "listening" period, where they optimize spend to ensure we are getting the best possible ROI. 

#### **Paid Display**: 
Display ads are banner ads that we mostly run are through the [Google Display Network](https://support.google.com/google-ads/answer/2404190?hl=en). Banner ads will show on websites that have [Google Adsense](https://support.google.com/adsense/answer/6242051?hl=en) set-up on their website. There are no specific websites we show banner ads on - we earn the ad space by bidding on placements based on specific targeting criteria such as demographics, topics, and interests. On occassion, we run banner ads on websites through direct buys. This is handled more in the publisher program. 
* **Best used for**: Top and mid funnel content. More used for awareness and some action based response.
     * **Best type of content to use**: How-to guides, ebooks, and general awareness content promotion do well here. Trial ads tend to do well if used in some for of remarketing strategy.
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports. Because there is a learning period when running Display advertising, running Events through Display advertising is not recommended due to the short promotion period. With leadership reports/whitepapers, we historically have not seen good engagement and sign-ups for these types of assets on Display advertising. 
* **Types of targeting we do**:
     * **Contextual targeting**: Show banners ads on websites that are related to the content of our landing page and website. This is done based on keyword and topic targeting.
     * **Prospecting targeting**: Show banner ads to related audiences that are similar to those who have converted on our website. A profile is developed based on people who convert. We would then show banner ads to people that closely match that profile. This method helps drive new traffic to GitLab that may not know about the brand or product. 
     * **Remarketing**: Show banner ads in order to re-engage people who have already visited pages on the GitLab website. This tactic can show ads on what seems to be irrelevant websites. However, targeting is based on the engagement of the person, not the context of a website. 

#### **Paid Social**:
Paid social ads are ads that we show on social platforms. The three social media platforms that we primarily advertise on are Facebook/Instagram, LinkedIn (this includes LinkedIn InMail & Conversation Ads), and Twitter. 
* **Best used for**: Top and mid funnel content. Does well for both awareness and direct response (depending on the asset used).
     * **Best type of content to use**: Live webcasts, recorded webcasts, events, and ebooks/guides. Live events with a sign-up deadline, tend to perform the best in paid social. 
     * **Worst type of content to use**: Thought leadership reports/whitepapers like Gartner and Forrester reports.
* **Types of targeting we do**:
     * **Prospecting targeting**: Sponsored ads to audiences that are built based on personas using interest, professional, and demographic targeting. A profile is developed based on people who convert. We would then show ads to people natively within either Facebook, Instagram or LinkedIn that closely match that profile. This method helps drive new traffic to GitLab that may not know about the brand or product. 
     * **Account targeting**: Sponsored ads on targeting speciific accounts or contacts based on a list upload or import from Marketo. LinkedIn is the top channel for this type of marketing based on a +90% account match rate, whereas Facebook and Instagram requires accounts to be manually entered in order to target.
     * **Remarketing**: Show  ads in order to re-engage people who have already visited pages on the GitLab website.

#### **Publisher Sponsorships**:
Publisher sponsorships are when we engage a specific publisher in order to purchase placement on their web properties. Generally, we make sure the publisher's website(s) and audience closely match the profile of who we want to advertise to before engaging with the publisher. Additionally, we want to make sure the programs that are offered by the publisher align with our goals. 
* **Best used for**: Primarily used for demand generation, so we focus on mid to bottom funnel content.
     * **Best type of content to use**: Live webcasts and recorded webcasts work the best. Ebooks and guides sometimes work, depending on the placement. 
     * **Worst type of content to use**: Events and thought leadership reports/whitepapers like Gartner and Forrester reports. With leadership reports/whitepapers, we historically have not seen good engagement and sign-ups for these types of assets on Paid Social, unless the report is more of a how-to guide. 
     
### Digital Campaign Design Specs
Each paid channel has its own unique design specifications and recommendations for their ad types to ensure ads can run at their optimal performance. If you do not yet have creative assets secured for your campaign, the design team can use this section as their guide when producing your creative.

#### Paid Social
* Facebook Image:
   - Recommended Image Size: 1200x628 pixels
   - Recommended Image Ratio: 1.91:1 to 4:5
   - Recommended Image File Type: JPG or PNG
   - No buttons allowed
   - Do not include text in the creative
* Facebook Video:
   - Recommended Video Ratio: 9:16 (full vertical) to 16:9 (feed/landscape)
   - Recommended Video File Type: MP4 or MOV
   - Recommended Video File Size: 4GB Max
   - Recommended Video Length: 5-15 seconds
   - Video Captions: Optional but recommended
   - Video Sound: Optional but recommended
* Facebook Carousel
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Image File Type: JPG or PNG
   - Recommended Video File Type: MP4, MOV or GIF
   - Recommended Ratio: 1:1
   - Number of Carousel Cards: 2 to 10
   - Image Maximum File Size: 30MB
   - Video Maximum File Size: 4GB
   - Video Duration: 1 second to 240 minutes
   - Aspect Ratio Tolerance: 3%
   - No buttons allowed
   - Do not include text in the creative
* Instagram Image
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Image File Type: JPG or PNG
   - Recommended Ratio: 1:1
   - Image Maximum File Size: 30MB
   - No buttons allowed
   - Do not include text in the creative
* Instagram Video:
   - Recommended Video Ratio: 1.91:1 to 4:5 supported
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Video File Type: MP4, MOV or GIF
   - Recommended Video File Size: 30MB Max
   - Recommended Video Length: 1 second to 2 minutes
   - Video Captions: Optional but recommended
   - Video Sound: Optional but recommended
* Instagram Carousel
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Image File Type: JPG or PNG
   - Recommended Video File Type: MP4, MOV or GIF
   - Recommended Ratio: 1:1
   - Number of Carousel Cards: 2 to 10
   - Image Maximum File Size: 30MB
   - Video Maximum File Size: 4GB
   - Video Duration: 1 second to 60 minutes
   - Aspect Ratio Tolerance: 1%
   - No buttons allowed
   - Do not include text in the creative
* LinkedIn Image:
   - Recommended Image Size: 1200x627 pixels
   - Recommended Image Ratio: 1.91:1
   - Recommended Image File Type: JPG or PNG
   - Recommended Image File Size: 5MB Max
   - Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
* LinkedIn Video: 
   - Recommended Video Length: Less than 15 seconds
   - Recommended Video File Size: 75 KB to 200 MB
   - Recommended Video File Format: MP4
* LinkedIn Carousel
   - Recommended Resolution: 1080x1080 pixels
   - Recommended Image File Type: JPG or PNG
   - Recommended Ratio: 1:1
   - Number of Carousel Cards: 2 to 10
   - Image Maximum File Size: 10MB
   - Video not supported currently
* Twitter Image: 
   - Recommended Image Size: 1200x675 pixels
   - Recommended Image Ratio: 1:1
   - Recommend using shorter copy within image, and lean on the headline/introduction text to convey more of your message
* Twitter Video:
   - Recommended Video Size: 1200x1200
   - Recommended Video Ratio: 1:1
   - Recommended Video Length: less than 15 seconds
   - Recommended Branding: Consistent in upper left-hand corner
   - Recommended Video File Type: MP4 or MOV
   - Recommended Video File Size: 1GB Max

#### Paid Display (Google Display Network)
* Image Sizes
   - 160x600
   - 250x250
   - 300x1050
   - 300x250
   - 300x600
   - 320x50
   - 336x280
   - 728x90
   - 970x250
* Recommended Image File Type: JPG or PNG
* Recommended Image File Size: 150KB Max
* In-Banner Design High Performers
   - Benefit/Value Prop and educational messaging
   - CICD emblem background
   - “Learn More” CTA
   - 4-7 word volume

## Requesting Digital Marketing Promotions

If you would like to request a paid digital marketing promotion in paid search, paid social, paid sponsorships or other paid marketing to support your event, content marketing, or webcast, asset, etc. create an issue in the [Digital Marketing Programs Repo](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/tree/master) and then follow the [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new?issuable_template=paid-digital-request). If you are specifically requesting a Demandbase campaign, create an issue with the [Demandbase Campaign Request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/blob/master/.gitlab/issue_templates/Demandbase_Campaign_Request.md).

If you request a digital marketing promotion you probably also need a marketing campaign and should consult with the [Marketing Program Managers](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/).

### Issue Items Defined

* **Campaign Name**: Name of the campaign that you are requesting
* **Campaign Budget**: How much budget you would like to allot to this campaign
* **Campaign/Finance Tag**: If you have a specific Campaign/Finance Tag that you need us to bill to, please input here. These will also be used as the `utm_campaign` name in reporting (minus spaces, underscores, and special characters)
* **Team Making Promotion**: Team that we should bill to (i.e. Field Marketing - AMER, Corporate Marketing, etc). 
* **Campaign Description**: Brief description of the campaign and what you are trying to achieve
* **Campaign Start and End Dates**: Dates of when the campaign should start and end
* **Campaign Goal**: What is the numeric goal and KPI for the campaign (registrations, page views, downloads, etc)
* **Campaign Target Audience**: The type of people that you want to reach in this campaign
* **Campaign Creative Asset**: If you have creative set already for this campaign, please provide links to the creative. If not, this may require a separate ask for Design team, please make request in Corporate project and factor into timeline

### DMP Request Workflows

**1. Paid Ads  (Search, Social, Display)** 
* Before creating an issue in Digital Marketing Programs, this information is required:
   * Budget
   * Start and end dates
   * General targeting parameters
   * Prepared account list (if applicable)
   * Geo targeting within certain regions
   * Landing Page URL(s)
   * Campaign/Finance tag
   * If you would like guidance from the DMP around certain items, please make a comment within the issue.
* Assign the issue to:
   * Leslie Stinson for Field, Microcampaigns, ABM, and Acceleration.
   * Matt Nguyen for Integrated Campaigns, non-campaign webcasts, and Corporate initiatives (such as All Remote).
* DMP will then create an issue for our digital agency PMG.
   * Only DMP can create issues within Digital Advertising (our project for PMG).
   * DMP will liaise between the campaign owner and the agency within separate issues so wires are not crossed.
   * Please do not comment in PMG issues within Digital Advertising unless you are added as a participant.
* DMP or MPM will create a design request issue if no creative is provided.
* DMP will approve copy & mockups.
   * PMG will generate copy & CTAs based on the landing page or content brief.
   * DMP will approve copy and mockups, unless approval from the campaign owner is requested.
* If testing is requested, DMP will secure testing variations (copy, CTA, and/or creative) and propose a schedule for testing rounds.
* * DMP will confirm launch within the issue.
* DMP will provide campaign summary and report after the campaign ends.

**2. Publisher Engagements (Content Syndication, Hosted Webinars, Lead Generation Packages, and Sponsorships)** 
* Before creating an issue in Digital Marketing Programs, this information is required:
   * Budget
   * Start and end dates
   * General targeting parameters (if applicable)
   * Landing Page URL(s)
   * Campaign/Finance tag
   * Along with Publisher description and contact information, please provide additional information for specific campaign type:
      * Sponsorships: ad placement information, including creative specs
      * Content Syndication: content to promote (PDF file recommended)
      * Lead Generation Packages: lead list information and upload & implementation strategy 
      * Hosted Webinar: webinar information, event date, presentation assets
   * If you would like guidance from DMP around certain items, please make a comment within the issue.
* Assign the issue to:
   * Leslie Stinson for Field, Microcampaigns, ABM, and Acceleration.
   * Matt Nguyen for Integrated Campaigns, non-campaign webcasts, and Corporate initiatives (such as All Remote).
* DMP will then create an issue for our digital agency PMG.
   * Only DMP can create issues within Digital Advertising (our project for PMG).
   * DMP will liaise between the campaign owner and the agency within separate issues so wires are not crossed.
* DMP or MPM will create a design request issue if creative is needed for this campaign.
* DMP will approve copy & mockups if needed for this campaign.
   * PMG will generate copy & CTAs based on the landing page or content brief.
   * DMP will approve copy and mockups, unless approval from the campaign owner is requested.
* DMP will confirm launch within the issue.
* Reporting will be provided via Publisher, and PMG can also provide Publisher Reporting through Google Data Studio.

### DMP Request Timing
* Time required to get a campaign into market
   * Ideally, we need at least 3-4 weeks before the proposed launch date in order to plan strategy, forecast goals, and secure all creative assets.
* Turnaround time for mock-ups (if applicable)
   * Once creative & copy are generated, PMG can produce ad mockups for review upon request.
* Turnaround time to populate audiences (if applicable)
   * If the campaign is targeting a named account list, it can take 24-48 hours to populate within a platform.
* Turnaround time for Design
   * Depending on the request size, this could take at least 5 business days. A request for 2 LinkedIn images will take much less time than a request for a fully integrated campaign that requires multiple display & paid social sizes.
* Timing required for publisher engagements (sponsorships)
   * This will depend primarily on the publisher response time. Once PMG is connected with the publisher contact, 
* Expectations for reporting
   * Current DataStudio report(s) - request from DMP



## LinkedIn InMail & Conversation Ad Campaigns

### InMail (Message Ads)

LinkedIn InMail Ads (also known as Message Ads) are static messages delivered directly to LinkedIn member inboxes, creating a personalized ad experience between the user and GitLab. These ads are highly effective for middle-funnel campaigns, targeting specific individuals with custom tailored content relevant to their persona and/or organization. Since many users also have LinkedIn notify their personal or work email address when they have received an InMail, this results in up to 2x higher open rates and click throughs than traditional single email campaigns. Unlike our main paid digital bid type of costs per click, we pay for the message open which generally costs around $1 - $1.50 each. However, when a user shares the ad to other users, those users' opens and clicks are free. As a result, the free sharing feature allows our campaigns to obtain greater reach at a lower cost. This is especially useful in Account Based Marketing campaigns; a manager receiving an InMail to attend an upcoming GitLab webinar can share with other users in their organization. InMail/Message Ads can be used to re-engage and nurture accounts with upcoming events, new content assets, or by simply opening the conversation. 

### Conversation Ads

Conversation Ads are similar to InMail Ads in terms of placement, costs, and targeting, but instead of a static message, the user is able to engage through a sequence of messages prompted by specific CTA buttons, creating an interactive experience with GitLab. Conversation Ads are delivered directly to the LinkedIn member’s inbox, starting off with an intro message offering a variety of CTA buttons and proceeding through a custom sequence with each button click, resulting in several layers of interactions. LinkedIn reporting features visualization into engagement through the message flow, highlighting which messages & CTAs users found most relevant and where conversation drops off. These CTAs can click through to the next message  (“Tell me more”), click out to a relevant landing page (“See case studies”), click out to an event registration page (“Register now”), or open an lead generation form (“Sign up now”). Conversation Ads are essentially a Choose Your Own Adventure interaction that provide target audiences with more options and provide us with more audience insights.

If you are requesting InMail or Conversation Ads, create an issue in the general [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=paid-digital-request).


### Sender Collaboration

InMail & Conversation Ad campaigns must have a designated sender in order to send messages on behalf of GitLab, putting a friendly face to the company name. The ads displayed in a member’s inbox will appear to be sent from the designated sender, which allows for increased personalization and message relevance to targeted members.

In order for our digital agency PMG to set up sender permissions, they’ll need to send requests to those whom we want to grant sender permissions, which are typically SDRs but can include a variety of titles & teams at GitLab. Since sender requests can only be sent to 1-degree connections, a member of the PMG team will first send a LinkedIn connection request, then the sender request once the connection is approved. When the designated team member receives a request to be a sender on a LinkedIn Messaging campaign, they can approve or reject this request by following the link provided in the email. The GitLab team member can also [use this link](https://www.linkedin.com/ad/accounts?destination=sponsored-inmail-sender-permissions) to manage their sender permissions and see whether they have sender access for a campaign. Once the GitLab team member has accepted their sender permissions to be added as a sender on the account, they will need to notify the DMP by confirming within the campaign issue.

### Copy


Although Digital Marketing can generate copy, we highly recommend that the sender, another member of the sender’s team, or a PMM generate the copy to be used in messages since they have more insight & context around targeted accounts, user/buyer persona, and/or the promoted content. When creating an InMail or Conversation Ad campaign, the Ad Copy Template doc (included for both ad types in the [Paid Digital Request template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=paid-digital-request)) will be cloned for your specific campaign and used as a collaborative space for the sender to generate messaging and other campaign members to edit where necessary. Conversation Ad copy will be more complex, as it includes several layers of messages and multiple CTA options for each layer. Please note that due to the high volume of messages within a given campaign, LinkedIn restricts replies from the target audience to the sender. If the sender prefers replies, they can include a link to their Calendly or email address in the message. One of the key benefits of InMail & Conversation Ads is that you can personalize copy with macros. Macros allow you to pull a member’s LinkedIn profile data and insert them dynamically into your ad for more personalization. Profile data options include first name, last name, job title, company name, and industry.

**InMail Copy Best Practices:**
* Customize the greeting with the member’s name 
* Refer to their job title  
* Try using the word “you"
* The copy is fewer than 1,000 characters
* The CTA is clear
* The landing page is optimized for mobile

Best performing subject lines often use some of the following keywords:
* Thanks 
* Exclusive invitation  
* Connect 
* Job opportunities  
* Join us  

Additional ways that we can test with message tone of voice:
* Genuine
* The Helpful Advisor
* VIP Invitation - sending personalized invites to 'exclusive' events
* The Cliffhanger

**Conversation Ad Copy Best Practices:**
* Always start with your main goal and/or objective: What is the ideal action you want the member to take when they receive your Conversation Ad? 
* To drive brand consideration: Link to your blog posts, pre-recorded webinars, or industry trends and analysis
* To drive leads and turn prospects into customers: Share product demos or tutorials, customer success stories, or invite prospects to attend an event
* Introduce yourself: When using an individual as a sender, use the opening message to introduce yourself and let members know why you’re reaching out
* There’s no subject line for Conversation Ads. Like any other LinkedIn message, the first sentence will appear as the subject. Because your audience will see this in their LinkedIn Messaging, make your first sentence count. 
* Use multiple messages & buttons: Your conversation should have 2 to 5 layers. Each layer will consist of message text and at least 2 CTA buttons with responses to the question in your message. For instance, if your goal is to drive content downloads, share two pieces of content that your audience can choose from. 
* Don’t include “Not Interested” or “No Thanks” CTAs: Avoid including this in the first layer of your conversation. Prospects who aren’t interested will just close the ad. Instead, focus on using CTAs they can learn from.
* Ask your email marketing team for ideas: Check in with your email marketing team to understand what works best for them. Use their top performing email copy as inspiration for your Conversation Ad. 
* Keep copy short & sweet: Conversation Ads are meant to feel more like a live conversation. Keep your messages short and friendly, and don’t forget about the character limits. Consider using language that is more casual, the way you would over the phone.
* Stick to a conversational tone: While you may be engaging a B2B audience, remember that it’s people who make the decisions to buy your product or service, and they want to know that your brand is human and authentic.


## UTMs for URL tagging and tracking
All URLs that are promoted on external sites and through email must use UTM URL tagging to increase the data cleanliness in Google Analytics and ensure marketing campaigns are correctly attributed. 

We don't use UTMs for internal links. UTM data sets attribution for visitors, so if we use UTMs on internal links it resets everything when the clicked URL loads. This breaks reporting for paid advertising and organic visitors.

You can access our internal [URL tagging tool in Google Sheets](https://docs.google.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit#gid=0). You will also find details in this spreadsheet on what "Campaign Medium" to use for each URL. If you need a new campaign medium, please check with the Digital Marketing Programs team as new mediums will not automatically be attributed correctly.

If you are not sure if a link needs a UTM, please speak with the marketer who is managing your campaign to ensure you are not interrupting the reporting structure they have in place.

UTM construction best practices:
- lowercase only, not camelcase
- alphanumeric characters only
- no spaces
- **Campaign Medium** covers general buckets like `paidsearch`, `social`, or `sponsorship` 
- **Campaign Source** names where the link lives. Examples include `ebook`, `twitter`, or `qrcode`
- **Campaign Name** describes a specific campaign. Try to add additional context like `reinvent`, `forrester`, and `bugbounty`
- **Campaign Content** differentiates ad types.
- **Campaign Term** identifies keywords used in a campaign


## Opt-Out of Seeing GitLab Digital Ads

We run digital ads on the following channels:

* Google (paid search and display)
* Facebook
* LinkedIn
* Twitter
* Demandbase (paid display)

Because everyone at GitLab works remotely, it makes it difficult to restrict ads from being shown to GitLabbers. In a brick and mortar environment, we can easily block IP addresses to accomplish this. Because everyone at GitLab has a different IP address, and even dynamic IP addresses, there is no way to implement an exclusion rule that would block all GitLab ads to GitLabbers. 

If you would not like to see GitLab ads, you are able to opt-out of ads as you see them. Below is the process to remove GitLab ads. Please note that if you use both your personal and work accounts on your devices, you will need to exclude from both your personal and work accounts. 

**Google Paid Search**: When you see a GitLab text ad in Google search engine results
1. On the ad, click on the arrow next to the URL
2. Click on Why this ad? 
3. On the toggle next to “Show ads from gitlab.com”, toggle off
4. Close out of box

**Google Display**: When you see a GitLab ad on a third-party website
1. Click on the blue arrow icon at the top right-hand corner for the ad 
2. Select “Stop seeing this ad” when the option appears
3. Make a selection on why you do not want to see the ad anymore
4. Done

**Facebook**: When you see a GitLab ad on Facebook
1. On the upper right-hand corner of the ad, click the three dots in that corner
2. When, menu appears, click on “Why am I seeing this ad?”
3. In the next pop-up box, under “What You Can Do”, click the “Hide” button next to the option that says “Hide all ads from this advertiser”

**LinkedIn**: When you see a GitLab ad on LinkedIn
1. On the upper right-hand corner of the ad, click the three dots in that corner
2. When option box appears, click on “Report this ad, I don’t want to see this ad in my feed”
3. In the pop-up box that appears next, select an option
4. Click the Submit button on the next page

**Twitter**: When you see a GitLab ad on Twitter
1. On the upper right-hand corner of the ad, click the arrow
2. When option box appears, click on “I don’t like this ad”
* Note that this does not necessarily block all ads, just that specific ad. You would need to do this on all the ads you see from GitLab
* You could block @gitlab in your profile to no longer see ads. However you block all ability to engage with the @gitlab Twitter accounts as well (seeing Tweets, Retweeting, mentioning, etc). 


