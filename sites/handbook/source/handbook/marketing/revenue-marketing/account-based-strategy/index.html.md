---
layout: handbook-page-toc
title: "Account Based Strategy"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Account Based Strategy

## Ideal Customer Profile
The account based marketing team is responsible for developing and managing our ideal customer profile.  An ideal customer profile is the description of our "perfect" customer company (not individual or end user).  The profile takes into consideration firmographic, environmental and additional factors to develop our target list of highest value accounts. 

A few things to note about our ICP: 
- it is that it is fairly broad, mainly because GitLab can ultimately sell to a vast number of companies versus say, a banking solution that would have a much smaller TAM (total addressable market)  
- our ICP is hyper focused on first order logos
- Because of our large TAM we do not target ALL the accounts that fit our ICP at a given time, but rather, focus on a subset based on different variables including propensity to buy and additional intent data.

### Large

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of developers (currently also using company size as proxy) | 500+ |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket or Subversion |
| | Cloud provider | AWS or GCP |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization)  |
| **Additional criteria (attributes to further define)** | Digital transformation | identified C-suite initiative | 
| | High intent account | Account is trending as high intent based on our data in Demandbase |

### Mid Market

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of employees | <150 & >650 |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket or Subversion.  Also include a LACK of tech stack in smaller companies |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization)  |
| **Additional criteria (attributes to further define)** | New hire | CIO | 
| | High intent account | Account is trending as high intent based on our data in Demandbase |

## GL4300 & MM4000
Target list of accounts that are first order logos (first paid customer within an organziation) for GitLab and fit our ideal customer profile for both Large (GL4300) and MidMarket (MM4000).  The lists are developed using the current data sources we have at our disposal and are refreshed quarterly.

* GL4300 stands for GitLab & the number of target accounts we are targeting. This list represents only those accounts in our [`Large` segment](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation). 
* MM4000 stands for MidMarket & the number of target accounts we are targeting. This list represents only those accounts in our [`MidMarket` segment](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#segmentation). 

### How we develop the list
We take from a variety of data sets currently but the ideal state is to have all data flowing to Salesforce as our single source for qualifying and scoring accounts.
1. accounts identified by sales as strategic by region and match our ICP
1. accounts in our database that match our ICP 
1. lookalike accounts: account that act, look & behave like our existing best customers
1. agreed upon sales & marketing identified strategic accounts

### Data sources
We use a variety of data sources to determine if an account matches one of our ideal customer profile data points.  The table below shows the order of operation of where we look for this data.

| Attribute | Data Sources (in order of priority) |
| ------ | ------ |
| Number of developers | Aberdeen number of developers --> user/SAL input in Salesforce --> No. of employees as a proxy |
| Technology stack | Aberdeen technology stack --> user/SAL input in Salesforce --> Zoominfo tech stack  |
| Cloud provider | Aberdeen technology stack --> user/SAL input in Salesforce --> Zoominfo tech stack |
| Prospect | Total CARR for all accounts within the hierarchy equals zero |

### When an account moves to/from our GL4300 & MM4000 target lists
Both target lists will be reviewed and modified during the last 2 weeks of each quarter.  The process is as follows:
##### T-minus two weeks from end of quarter
1. ABM nominations are made by sales

##### Last day of quarter
1. Closed won logos will be filtered out and the lists will be refilled based on next up ideal customer profile accounts. 
1. Accounts not showing an increase or are showing a decrease in engagement will be removed from the target list
1. Any additional accounts identified by sales as not a good target due to lack of budget, etc

##### First business day of new quarter
1. Salesforce is updated with all changes to the target acccount list and shared with the organization

## Accounts are identified in Salesforce by the GTM strategy field in Salesforce:

**Volume**
Default selection for all accounts.

**ICP TOTAL ADDRESSABLE MARKET**
All accounts that fit our ideal customer profile.

**ACCOUNT CENTRIC**
Indicates that an account is a focus for field marketing and account centric campaigns (GL4300 and MM4000).

**ACCOUNT BASED- net new and ACCOUNT BASED- expand**
This defines that an account is included in one of the three tiers of our account based strategy.  `Account based- net new` will be included in the GL4300 as those are first order logo accounts.  `Account based- expand` will not as those are connected new or expand opportunities.  If an account is identified with either of these, the additional field of `ABM Tier ` that will be completed as well which will identify which tier the account falls into in respect to our account based strategy.

**ABM Tier**
This field is a sub field of GTM Strategy and will be populated if `Account Based` is chosen in the `GTM Strategy` field.  This field will identify which tier an account is currently in.

## Account Sources
All accounts in Salesforce that are part of the `ICP Total Addressable Market` will also have the `Net New Logo Target Account` field completed.
**Existing** An account that already existed in our circumstances

**Core** Newly identified core user

**Lookalike** Account identified based on attributes that match our exisitng customer base

### Aberdeen Data
As part of the development of our ideal customer profile, we purchased data from [Aberdeen](https://www.aberdeen.com/?gclid=Cj0KCQiAqdP9BRDVARIsAGSZ8AlzfX6vYnVNh7YX2IKrc6uNhqjfGY6sQywcyZalJScxTyexilB0pa4aAvFdEALw_wcB) to help us determine our ICP total addressable market.  The data included number of developers, specific technologies installed, and cloud provider.  The data is rolls up to the `Ultimate Parent` as we are looking for both the best entry point for an account and the overall environment.

| Data point | Salesforce field | Description & how to use the data |
| ------ | ------ | ------ |
| Number of developers | `Aberdeen Ultimate Parent Developer Count`  | This number is th total number of current developer contacts that Aberdeen has in their database for all sites of a company.  Because it is impossible to have a database of ALL contacts at a company, we look to this data point to verify if an account has over 500 developers IF the account has a number >500 in this field but we don't exclude an account from our TAM if thecount is lower than 500 due to the nature of the data point, rather, we go to our next best data point to verify. |
| Competitive technology | `Aberdeen Ultimate Parent Technology Stack` | This field identifies if a company has a certain technology in their technology stack that is part of our ideal customer profile |
| Cloud provider | `Aberdeen Ultimate Parent Cloud Provider` | Tells us if an account has AWS, GCP, or both as their cloud provider. |


## Where can I look to see the GL4300 and MM4000?
Both the GL4300 and MM4000 are identified in Salesforce by the `GTM Strategy` field = `Account Centric`.  From the you can filter by segment to see the respective lists, or by account owner etc to filter further.

## GL4300 and MM4000 FAQ
**What does new first order mean?** 
[Handbook page](/handbook/sales/#first-order-customers)
A new first order customer is a customer within an account family that purchase the first PAID subscription for that account family.

A connected new customer (sometimes called net new logo) is the first new subscription order with an Account that is related to an existing customer Account Family (regardless of relative position in corporate hierarchy) and the iACV related to this new customer is considered "Connected New".
Our GL4300 and MM4000 are focused on landing first order customers, therefore will not include connected new or expansion of any existing customer.

There is a field called `Order Type` in Salesforce that on the back end automatically captures New - First Order, Connected New, and Growth. Marketing is currently focused on increasing New - First Order.

**What does the Demandbase intent score mean?**
[Demandbase handbook page](/handbook/marketing/revenue-marketing/account-based-marketing/demandbase/)
You can read all about Demandbase and how they score accounts (both numerically and by H/M/L) in the handbook page dedicated to Demandbase.

**If a subsidiary of a conglomerate is a customer, is the parent (and all other subsidiaries) considered a customer (i.e. not net new logo)?**
Yes, as our 2H plan is focused on NEW FIRST ORDER therefore if a child account is a customer, then an account would not be included. These would be considered Connected New.

**As we run our campaigns against this list and learn more about which accounts are engaging/not, is there a process to take this learning into account for removing/adding accounts to the list?**
The GL4300 and MM4000 are both dynamic audiences. When an account is closed won/disqualified, it will be dropped from the list and refilled 1x a quarter ahead of sales QBR's.

**For accounts outside the list that are engaging, how can we share that information so they can get added to the list?**
We are using Demandbase to look track all accoutns that fit our ideal customer profile.  We can see an increase in engagement in the platform and will be adding to the list quarterly.

**At a high level, how does the intent data get collected?**
[Handbook page](/handbook/marketing/revenue-marketing/account-based-marketing/demandbase/#intent-fields--definitions) 

