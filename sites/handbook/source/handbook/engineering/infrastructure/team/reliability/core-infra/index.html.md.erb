---
layout: handbook-page-toc
title: "Core Infra Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Who We Are

The Core Infra teams owns:
* infrastructure tooling like:
  * [Terraform](https://gitlab.com/gitlab-com/gitlab-com-infrastructure)
  * [Chef](/handbook/engineering/infrastructure/production/architecture/#chef-architecture)
* network ingress/egress
  * [Networking](/handbook/engineering/infrastructure/production/architecture/#internal-networking-scheme)
* [CDNs and DNS](/handbook/engineering/infrastructure/production/architecture/#dns--waf)
* [Secrets management](/handbook/engineering/infrastructure/production/architecture/#secrets-management) 
* Helping with Fulfillment Stage assets like subscription and license management apps.

Need help?  [How to get something on our backlog](#asks-of-the-infra-team-and-unplanned-work)

<%= partial "handbook/engineering/infrastructure/team/reliability/core-infra/_core_infra_team.html" %>

## Tenets
Our guiding principles center around implementing [boring solutions](https://about.gitlab.com/handbook/values/#boring-solutions) to infrastructure problems. We work to simplify interfaces and build robust workflows for other engineers within GitLab who utilize our platform to provide support for and deliver new features to the gitlab.com SaaS product. Over time this continues to expand to include additional/related applications, sites, and systems.

In practice, this means that we 
1. Manage all infrastructure resources via Infrastructure as Code or auditing tools
    * GCP [by Terraform](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure)
    * Cloudflare / CDN [audited by a tool](https://ops.gitlab.net/gitlab-com/gl-infra/cloudflare-audit-log)
1. Develop automated workflows ensuring consistency, repeatability, predictability, and reliability
1. [Use GitLab to deploy and manage our infrastructure](https://about.gitlab.com/handbook/values/#dogfooding)
1. [Maintain a user-centric focus](https://about.gitlab.com/handbook/values/#customer-results), and [enable self-service functionality](https://about.gitlab.com/handbook/values/#self-service-and-self-learning) whenever possible
1. Work with other SRE teams to ensure common principles are upheld and services are maintained according to our [production readiness standards](https://gitlab.com/gitlab-com/gl-infra/readiness)
1. [Focus on small changes](https://about.gitlab.com/handbook/values/#iteration) and [embrace experimentation](https://about.gitlab.com/handbook/values/#accepting-uncertainty)


## Vision
* Help with moving compute to GKE
  * Smaller Chef footprint
  * More Autoscaling 
  * Secure and setup best practices for GKE cluster
* Create tools and processes for external teams to service requests themselves
* Move all Growth Stage apps to AutoDevops

## How we work:

#### Epics
We have 3 types of epics:
1.  The [main team epic](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/326).  This stores the roadmap/radar for the team. 
There are then lists of epics of the next 2 types for what we are working on now, what is up next and what is on the radar.
2.  Grouping or Backlog Epics (Themes).  These epics are things on our radar/roadmap. They are meant to group together related areas of work.
They are not commitments to work.  They are a way to help organize related areas into themes on our backlog.
3.  Project Epics.  These will usually sub-epics of the Grouping or Backlog Epics.  We'll create these epics for work that is 
["Up Next"](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/326#up-next-arrow_forward).  These epics will be used to 
scope the work of what is next and will be in progress.

#### Milestones
We use two types of milestones that can overlap:
1. Planned work milestones.  These correspond to the Project Epics above.  They give us a timebox in which we plan to do the work.
We can then watch burndowns for that work.
2. Unplanned milestones.  These are short 2 week timeboxes in which we schedule external/unplanned asks of the team that were
not in Project Epics.  They allow us to help set a budget for the incoming requests we do in a given timebox.  

#### Workflow
* We use a [Kanban board](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1688496?milestone_title=%23started&label_name[]=team%3A%3ACore-Infra) to track work
* If you assign yourself to an issue, it goes in a Milestone, gets the team::CoreInfra label. It will then appear on the Board.
* We use a Geekbot standup daily to talk about what we are working on
* We do Core-Infra team retros monthly async via Geekbot put into the #sre_coreinfra channel

#### Asks of the infra team and unplanned work:
When external asks come in:
* they can be commented on, but get a Workflow-Infra::triage Label to start
* When added to the board, the should get labels ~unplanned, AssistType::x and AssistingTeam::x, and be assigned to the next
  unplanned work milestone.

### Planning

We conduct our planning and retrospectives asynchronously, using issues in the [sre-coreinfra/planning](https://gitlab.com/gitlab-com/gl-infra/sre-coreinfra/planning) project to organize discussion.

#### Cadence

Planning occurs on a two-week cadence, aligned with our unscheduled milestones.

During the first week of a milestone, the team will review and discuss the _next_ upcoming milestone in an issue opened on the [sre-coreinfra/planning](https://gitlab.com/gitlab-com/gl-infra/sre-coreinfra/planning) backlog. This allows for time before the start of the next milestone to work on collecting additional data required to refine an issue, clarify details with requestors, or perform basic analyses of the relative size/scope of an issue _before_ starting the actual work.

During the second week of a milestone, in addition to resolving any questions or discussions raised on the next milestone planning issue, the team begins the issue triage process, and populates the planning issue for discussion when the cycle repeats.

#### Issue triage

Issue triage starts with reviewing rollover (unplanned issues in the current unplanned milestone that were not finished), incoming, unscheduled, and unprioritized work. Since rollover work is the least predictable in the first week of the milestone, planning is conducted with the assumption that all work in a milestone will be completed, and any rollover work will push a subsequent volume of work out to following milestones according to priority.

Aside from rollover issues, the remaining backlog issues will be reviewed in the following order
1. Work deferred due to rollover is still accurately prioritized and scheduled in the appropriate milestone
1. Open issues labeled `team::Core-Infra` and `workflow-infra::triage` will have an initial `core-infra::pX` priority added and move to `workflow-infra::ready`
1. New issues labeled `team::Reliability`

#### Planning discussion

Once the list of proposed issues for the upcoming milestone is completed, the team will perform a final review and discuss any additional context, missing details, pre-requisites, conflicts, etc. and decide on a final relative priority for work to be pulled.

#### Retrospectives

At the conclusion of each milestone, a retrospective issue will be created in the [sre-coreinfra/planning](https://gitlab.com/gitlab-com/gl-infra/sre-coreinfra/planning) project, for discussion and review. The text of the retrospective can be customized to ensure that certain aspects of the previous project/milestone are discussed, but in general will follow the format

1. What went well?
1. What did not go well?
1. What did you learn?
1. What still puzzles you?

### Labels

The Core-Infra team routinely uses the following set of labels:

1. The team label, `team::Core-Infra`.
1. Priority labels  `Core-Infra::P1` through `Core-Infra::P4`.
1. Scoped `workflow-infra` labels.
1. Scoped `Service` labels.

The `team::Core-Infra` label is used in order to allow for easier filtering of
issues applicable to the team that have group level labels applied.

<a name="priority-labels">The priority labels</a> allow us to track the issues correctly and raise/lower priority of work based on both external and internal factors.

This means that the highest priority is given to working on issues that improve
Gitlab.com SLO's either immediately and directly, or by unblocking other issues
to achieve the same.

#### Workflow labels

The Core-Infra team leverages scoped workflow labels to track different stages of work.
They show the progression of work for each issue and allow us to remove blockers or change
focus more easily.

The standard progression of workflow is from top to bottom in the table below:

| State Label | Description |
| ----------- | ----------- |
| `workflow-infra::Triage` | Problem is identified and effort is needed to determine the correct action or work required. |
| `workflow-infra::Proposal` | Proposal is created and put forward for discussion and review. <br/>SRE looks for clarification and writes up a rough high-level execution plan if required. SRE highlights what they will check and along with soak/review time and developers can confirm. <br/>If there are no further questions or blockers, the issue can be moved into "Ready". |
| `workflow-infra::Ready` | Proposal is complete and the issue is waiting to be picked up for work. |
| `workflow-infra::In Progress` | Issue is assigned and work has started. <br/>While in progress, the issue should be updated to include steps for verification that will be followed at a later stage.|
| `workflow-infra::Under Review` | Issue has an MR in review. |
| `workflow-infra::Verify` | MR was merged and we are waiting to see the impact of the change to confirm that the initial problem is resolved. |
| `workflow-infra::Done` | Issue is updated with the latest graphs and measurements, this label is applied and issue can be closed. |

There are three other workflow labels of importance:

| State Label | Description |
| ----------- | ----------- |
| `workflow-infra::Cancelled` | Work in the issue is being abandoned due to external factors or decision to not resolve the issue. After applying this label, issue will be closed. |
| `workflow-infra::Stalled` | Work is not abandoned but other work has higher priority. After applying this label, team Engineering Manager is mentioned in the issue to either change the priority or find more help. |
| `workflow-infra::Blocked` | Work is blocked due external dependencies or other external factors. Where possible, a [blocking issue](https://docs.gitlab.com/ee/user/project/issues/related_issues.html) should also be set. After applying this label, issue will be regularly triaged by the team until the label can be removed. |


#### Priority labels

The Core-Infra team uses priority labels as a means to indicate order under which work is next to be picked up. Priorities are roughly defined as:

| Priority level  | Definition |
| --------------- | ---------- |
| `Core-Infra::P1` | Issue is blocking other team-members, or blocking other work.  As soon as possible after completing ongoing task unless directly communicated otherwise. |
| `Core-Infra::P2` | Issue has a large impact, or will create additional work. |
| `Core-Infra::P3` | Issue should be completed once other urgent work is done. |
| `Core-Infra::P4` | **Default priority**. A nice-to-have improvement, non-blocking technical debt, or a discussion issue. |


