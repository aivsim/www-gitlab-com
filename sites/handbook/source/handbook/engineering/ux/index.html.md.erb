---
layout: handbook-page-toc
title: "UX Department"
description: "The GitLab UX department is comprised of four areas to support designing the GitLab product: UX Research, Product Design, Technical Writing, and UX Foundations"
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hello!

We're the GitLab User Experience (UX) department. We're comprised of four areas to support designing and building the GitLab product. 

* [UX Research](/handbook/engineering/ux/ux-research/)  
* [Technical Writing](/handbook/engineering/ux/technical-writing/)  
* [Product Design](/handbook/engineering/ux/product-design/)  
* UX Foundations - [Pajamas](https://design.gitlab.com)  

Our goal is to make our product easy to use, supportive of contributions from the wider GitLab community, and built for a diverse global community. We want GitLab to be the easiest and most delightful product in its class.

We hope you find what you are looking for here. If you don't, please open an [issue](https://gitlab.com/gitlab-org/gitlab-design/issues/new) and give us feedback.


## FY21 Direction
Our direction for fiscal year 2021 is to offer a best-in-class user experience for all of the DevOps categories in which we compete. 

It's important to note that, even though we're internally organized into teams that compete with specific industry segments, we can't design in silos. Because we're a single application for the DevOps lifecycle, we must strive to create experiences that flow seamlessly across the entire product. Instead of thinking about individual features, we must always consider the end-to-end job that a user wants to get done.

We measure the quality of our user experience in a variety of ways. A few examples are our quarterly [System Usability Scale survey](/handbook/engineering/ux/ux-resources/#system-usability-score), [UX Scorecards](/handbook/engineering/ux/ux-scorecards/), and [Category Maturity Scorecards](/handbook/engineering/ux/category-maturity-scorecards/). We're actively working to improve our SUS score (which was [flat during FY20](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)) by emphasizing solution validation and resolving UX debt. We're continuing to evaluate current experiences within the product with UX Scorecards, and we're also validating Category Maturity Scorecards to ensure that the wider GitLab community has input to our [category maturity](https://about.gitlab.com/direction/maturity/) ratings.

Our UX and Product Management departments both understand that creating a great user experience is only possible when we actively solicit feedback from users. That's why we have a robust UX Research program that is guided by our [research team](/handbook/engineering/ux/ux-research/). They help our product managers and designers conduct extensive research, including (but not limited to) problem and solution validation. 

Based on experience, we know that having a robust Design System that offers single-source-of-truth components is a great way to increase productivity, drive UI consistency, and improve our visual design. Accordingly, we're working hard to make [Pajamas](https://design.gitlab.com/) a robust system that's customized to our unique needs. During FY21, we're adding a dedicated UX Foundation team to guide this work, making sure our UI components are beautiful, scalable, and accessible. While we'll have a small dedicated team, it's still imperative for every product designer to contribute to Pajamas.

In FY21, we're adding additional leadership to our rapidly growing department to maintain appropriate manager/direct report ratios that ensure everyone has the support they need. Research and Technical Writing will both have a Senior Manager and multiple team managers, and Product Design will also add two senior leaders to help guide the strategy led by the UX Managers of our product sections.

When we hire UXers, we look for practioners who have existing experience in the subject matter areas they will support. Because GitLab is a complex product, our goal is to set new UXers up for success, and existing domain knowledge makes that process easier.

Additionally, we hire UX generalists who are experienced in multiple areas. For example, our Product Designers are strong UI and visual designers, they can conduct their own research, they're experienced in defining and communicating UX strategy, and they sometimes have front-end development expertise. Similarly, our Technical Writers create great documentation, but they also know how to write compelling UI text and manage docs as code.


## Our Strategy
### We support all users from beginners to experts
We believe that GitLab software should be unintimidating and accessible for a beginner, without oversimplifying important features for advanced users. We stay with users every step of the way to help them learn fast as a beginner and then become an expert over time. 

### We're building one product, together
We're highly focused on ensuring that no matter how big our product gets, the entire experience stays cohesive, consistent, and interconnected.

### We're humble facilitators of user experience design
Everyone is a designer; everyone can contribute. We are not egotistical, moody experts who alone hold the keys to user delight. We encourage Product Managers, Engineers, and the wider GitLab community to contribute to creating an exceptional user experience. 

### We look for small changes and big impacts
Sometimes the simplest, most boring solution is what is needed to make users successful. We want our UI to stay out of the user’s way. We work iteratively to make modest but valuable changes that make users more productive, faster, and better at accomplishing their tasks.

**NOTE:** When we find problems that are simple to fix - for example, a minor change to our website or microcopy in the product - we are empowered to make those changes ourselves. Example: If the change will take you less than 15 minutes to make, then start with an MR instead of an issue. By making the change yourself, you are taking immediate action to improve our product, and you might learn a new skill, too! If it seems simple, but you have questions, remember that there are people who can help you with code changes both in the UX department and across the company. (Even Sid is willing to help, if you need it.)

### We're informed by empathy
We’re human, and we design for humans, so we strive for understanding, self-awareness, and connection. We are quirky, and we introduce our quirks into designs when appropriate.


## Personas we use

Existing personas are documented within the [handbook](/handbook/marketing/strategic-marketing/roles-personas/).

New personas or updates to existing personas can be added at any time.

Personas should be:

* Informed by research.
* Driven by job title or feature.
* Gender neutral.

## Areas of Responsibility 

* **Pajamas Design System:** To ensure that everyone can contribute to GitLab with confidence we provide everyone with the right resources and know-how. The [Pajamas](https://design.gitlab.com/) design system is the single source of truth for everything anyone needs to know about contributing to GitLab. The UX Department owns the visual and interaction design, as well as implementation of Pajamas.
* **Navigation of GitLab UI:** Navigation is an extremely important part of the user experience. Our goal is keeping the navigation architecture intelligible, comprehensible and making sure it serves every user need. [Learn how to make changes to navigation](/handbook/engineering/ux/navigation/). 
* **UX Scorecards:** As we grow our platform, we want to keep evaluating user experience of various user tasks and flows to make sure we are tracking progress and improvements over time. [UX Scorecards](/handbook/engineering/ux/ux-scorecards/) is our framework for achieving this goal. 
* **Category Maturity Scorecards:** We listen to our users and grade the maturity of our product based on user performance and feedback. [Category Maturity Scorecards](/handbook/engineering/ux/category-maturity-scorecards/) is the methodology we use for these evaluations.
* **Technical Documentation:** Our users need reliable documentation, as it helps keep track of all aspects of a platform and it improves on the quality of a software product. We manage [docs.gitlab.com](https://docs.gitlab.com/) as well as related processes and tooling.
* **First Look:** Inviting users into everything we do is very important to us in order to be able to collect feedback. [First Look](https://about.gitlab.com/community/gitlab-first-look/) is our user engagement and recruiting program, which enables us to connect with our users and get their thoughts on our product.
* **UX Showcase:** Collaboration is one of our values. <%= data.ux_showcase.about %>

# How we work
Learn [how we work](/handbook/engineering/ux/how-we-work/) within our department and with cross-functional partners. 
## Meet Some of Our Team Members

This section is inspired by the recent trend of Engineering Manager READMEs. _e.g.,_ [Hackernoon: 12 Manager READMEs (from some of the best cultures in tech)](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe). Get to know more about the people on our team!

* [Christie Lenneville](/handbook/engineering/ux/one-pagers/christie-readme/) - VP of User Experience
* [Valerie Karnes](https://gitlab.com/vkarnes/readme) - Director of Product Design
* [Sarah Jones](/handbook/engineering/ux/one-pagers/sarahod-readme/) - UX Research Manager
* [Jacki Bauer](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md) - Product Design Manager
* [Justin Mandell](https://gitlab.com/jmandell/readme) - Product Design Manager
* [Taurie Davis](https://gitlab.com/tauriedavis/readme/blob/master/README.md) - Product Designer Manager
* [Rayana Verissimo](https://gitlab.com/rverissimo/readme) - Sr. Product Designer
* [Matej Latin](https://gitlab.com/matejlatin/focus) - Sr. Product Designer
* [Kyle Mann](https://gitlab.com/kmann/introspection) - Sr. Product Designer
* [Iain Camacho](https://gitlab.com/icamacho/koda/blob/master/README.md) - Sr. Product Designer
* [Jeremy Elder](https://gitlab.com/jeldergl/view/blob/master/README.md) - Sr. Product Designer, Visual Design
* [Nadia Sotnikova](https://gitlab.com/nadia_sotnikova/tasks/blob/master/README.md) - Product Designer
* [Austin Regnery](https://gitlab.com/aregnery/dear-journal/-/blob/master/README.md) - Product Designer
* [Anne Lasch](https://gitlab.com/alasch/about-anne/-/blob/master/README.md) - Sr. User Experience Researcher

[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
[everyone-designer]: https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434
[pajamas]: https://design.gitlab.com

## Join our UX Team

Are you interested in joining our team or hearing about new roles that open up within our department? Fill in [this short form](https://boards.greenhouse.io/gitlab/jobs/4700367002?gh_src=d865c64f2us) to join our talent community.
