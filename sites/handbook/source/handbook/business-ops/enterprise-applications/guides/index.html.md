---
layout: handbook-page-toc
title: "Enterprise Application Guides"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

One of the team's initiatives has been to conduct short training sessions on different topics within the Enterprise Applications domain.

**Why we do this:**
* Increase the base level of system and process knowledge across the team
* Provide a more homogenous level of support to our business partners on key topics
* Allow team members to self-service as much as possible and reduce disruptions for small tasks

## Finance System Guides
* [How to create custom fields in Netsuite](./finance-guides/#how-to-create-custom-fields-in-netsuite)
* [BambooHR Integration with Finance Systems](./finance-guides/bhrfinanceautomation)
